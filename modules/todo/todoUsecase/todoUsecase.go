package todousecase

import (
	"golang-darknet/config"
	"golang-darknet/models"
	todorepository "golang-darknet/modules/todo/todoRepository"
)

type ITodousecase interface {
	Prediction(base64 *models.Base64str) (*map[string][]models.Result, error)
}

type todousecase struct {
	cfg            config.IConfig
	todorepository todorepository.ITodorepository
}

func TodoUsecase(cfg config.IConfig, todorepository todorepository.ITodorepository) ITodousecase {
	return &todousecase{
		cfg:            cfg,
		todorepository: todorepository,
	}
}

func (u *todousecase) Prediction(base64 *models.Base64str) (*map[string][]models.Result, error) {
	predict_result, err := u.todorepository.Prediction(base64)
	if err != nil {
		return nil, err
	}
	return predict_result, nil
}
