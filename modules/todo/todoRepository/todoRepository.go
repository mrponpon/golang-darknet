package todorepository

import (
	"golang-darknet/config"
	"golang-darknet/models"
	"golang-darknet/modules/todo/darknet"
	"golang-darknet/utils"
)

type ITodorepository interface {
	Prediction(base64 *models.Base64str) (*map[string][]models.Result, error)
}

type todorepository struct {
	cfg config.IConfig
}

func Todorepository(cfg config.IConfig) ITodorepository {
	return &todorepository{
		cfg: cfg,
	}
}

func (t *todorepository) Prediction(base64 *models.Base64str) (*map[string][]models.Result, error) {
	img := utils.Base64toimg(base64.Base64)
	net := darknet.InitDarknet(t.cfg.AI())
	predict_result := net.Predict(img)
	return predict_result, nil
}
