package todohandler

import (
	"golang-darknet/config"
	"golang-darknet/entities"
	"golang-darknet/models"
	todousecase "golang-darknet/modules/todo/todoUsecase"

	"github.com/gofiber/fiber/v2"
)

type ITodohandler interface {
	Prediction(c *fiber.Ctx) error
}

type todohandler struct {
	cfg         config.IConfig
	todousecase todousecase.ITodousecase
}

func TodoHandler(cfg config.IConfig, todousecase todousecase.ITodousecase) ITodohandler {
	return &todohandler{
		cfg:         cfg,
		todousecase: todousecase,
	}
}

func (h *todohandler) Prediction(c *fiber.Ctx) error {
	req := new(models.Base64str)
	err := c.BodyParser(req)
	if err != nil {
		return entities.InitResponse(c).Error(
			fiber.ErrBadRequest.Code,
			err.Error(),
		).Res()
	}
	result, err := h.todousecase.Prediction(req)
	if err != nil {
		return entities.InitResponse(c).Error(
			fiber.ErrInternalServerError.Code,
			err.Error(),
		).Res()
	}
	return entities.InitResponse(c).Success(fiber.StatusOK, result).Res()

}
