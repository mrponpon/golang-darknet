package darknet

import (
	"fmt"
	"golang-darknet/config"
	"golang-darknet/models"
	"image"

	godarknet "github.com/LdDl/go-darknet"
)

type IDarknet interface {
	Predict(src image.Image) *map[string][]models.Result
	GetAI() *darknet
}

type darknet struct {
	configpath string
	weightpath string
}

func InitDarknet(cfg config.IAIconfig) IDarknet {
	return &darknet{
		configpath: cfg.Config(),
		weightpath: cfg.Weight(),
	}
}

func (d *darknet) Predict(src image.Image) *map[string][]models.Result {
	var result = make(map[string][]models.Result)
	n := godarknet.YOLONetwork{
		GPUDeviceIndex:           0,
		NetworkConfigurationFile: d.configpath,
		WeightsFile:              d.weightpath,
		Threshold:                .25,
	}
	if err := n.Init(); err != nil {
		fmt.Printf("init error: %v", err)
		return nil
	}

	imgDarknet, err := godarknet.Image2Float32(src)
	if err != nil {
		panic(err.Error())
	}

	dr, err := n.Detect(imgDarknet)
	if err != nil {
		fmt.Printf("detect error: %v", err)
		return nil
	}
	imgDarknet.Close()
	for _, d := range dr.Detections {
		for i := range d.ClassIDs {
			bBox := d.BoundingBox
			result[d.ClassNames[i]] = append(result[d.ClassNames[i]], models.Result{Probability: d.Probabilities[i], Startpoint: []int{bBox.StartPoint.X, bBox.StartPoint.Y}, Endpoint: []int{bBox.EndPoint.X, bBox.EndPoint.Y}})
		}
	}
	n.Close()
	return &result

}
func (d *darknet) GetAI() *darknet {
	return d
}
