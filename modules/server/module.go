package server

import (
	todohandler "golang-darknet/modules/todo/todoHandler"
	todorepository "golang-darknet/modules/todo/todoRepository"
	todousecase "golang-darknet/modules/todo/todoUsecase"

	"github.com/gofiber/fiber/v2"
)

type IModuleFactory interface {
	TodoModule()
}

type moduleFactory struct {
	r fiber.Router
	s *server
}

func InitModule(r fiber.Router, s *server) IModuleFactory {
	return &moduleFactory{
		r: r,
		s: s,
	}
}

func (m *moduleFactory) TodoModule() {
	repository := todorepository.Todorepository(m.s.cfg)
	usecase := todousecase.TodoUsecase(m.s.cfg, repository)
	handler := todohandler.TodoHandler(m.s.cfg, usecase)
	router := m.r.Group("/ai")
	router.Post("/prediction", handler.Prediction)
}
