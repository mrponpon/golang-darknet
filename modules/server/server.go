package server

import (
	"encoding/json"
	"golang-darknet/config"
	"log"
	"os"
	"os/signal"

	"github.com/gofiber/fiber/v2"
)

type IServer interface {
	GetServer() *server
	Start()
}

type server struct {
	app *fiber.App
	cfg config.IConfig
}

func InitServer(cfg config.IConfig) IServer {
	return &server{
		cfg: cfg,
		app: fiber.New(fiber.Config{
			AppName:      cfg.App().Name(),
			BodyLimit:    cfg.App().BodyLimit(),
			ReadTimeout:  cfg.App().ReadTimeout(),
			WriteTimeout: cfg.App().WriteTimeout(),
			JSONEncoder:  json.Marshal,
			JSONDecoder:  json.Unmarshal,
		}),
	}
}

func (s *server) GetServer() *server {
	return s
}

func (s *server) Start() {
	v1 := s.app.Group("v1")
	module := InitModule(v1, s)
	module.TodoModule()
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	go func() {
		_ = <-c
		log.Println("server is shutting down...")
		_ = s.app.Shutdown()
	}()

	// Listen to host:port
	log.Printf("server is starting on %v", s.cfg.App().Url())
	s.app.Listen(s.cfg.App().Url())
}
