package main

import (
	"golang-darknet/config"
	"golang-darknet/modules/server"
	"os"
)

func envPath() string {
	if len(os.Args) == 1 {
		return ".env"
	} else {
		return os.Args[1]
	}
}

func main() {
	cfg := config.LoadConfig(envPath())
	// ai := darknet.InitDarknet(cfg.AI())
	// ai.LoadDarknet()
	server.InitServer(cfg).Start()
}
