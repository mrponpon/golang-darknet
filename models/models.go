package models

type Users struct {
	ID       string `bson:"_id,omitempty"`
	Name     string `json:"name" form:"name"`
	Email    string `json:"email" form:"email"`
	Password string `json:"password" form:"password"`
}

type Result struct {
	Probability float32 `json:"prob"`
	Startpoint  []int   `json:"startpoint"`
	Endpoint    []int   `json:"endpoint"`
}

type Base64str struct {
	Base64 string `form:"base64"`
}
